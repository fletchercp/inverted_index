extern crate inverted_index;
extern crate time;

use std::fs::{File};
use std::io::{BufReader};
use std::io::prelude::*;

use inverted_index::index_store::Index;

// #[test]
// fn index_shakespeare() {
//     let _start = time::PreciseTime::now();
//     let mut buf = String::new();
//     let mut fh = File::open("tests/shakespeare.txt").unwrap();
//     let _result = fh.read_to_string(&mut buf);
//     let mut idx = inverted_index::index_sqlite::SQLiteIndex::new("shakespeare", "/tmp/rusticsearch").unwrap();
//     let doc = inverted_index::document::Document::new(&buf);
//     let _result = idx.insert_document(doc);
//     let _end = time::PreciseTime::now();
// }

// #[test]
// fn index_user_data() {
//     let _start = time::PreciseTime::now();
//     let fh = File::open("tests/user_data.txt").unwrap();
//     let file = BufReader::new(&fh);
//     let mut counter = 0;
//
//     let idx = inverted_index::index::Index::new("users", "/tmp");
//
//     match idx {
//         Ok(mut index) => {
//             for line in file.lines() {
//                let l = line.unwrap();
//                let doc = inverted_index::document::Document::new(&l.to_string());
//                let _ = index.add_document(counter, doc);
//                counter += 1;
//             };
//             index.flush_buffer();
//             let _end = time::PreciseTime::now();
//             let result = index.find_term("Tammie");
//         },
//         Err(e) => {
//             println!("Error creating new index: {:?}", e);
//         }
//     }
// }
