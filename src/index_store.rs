/// This Trait can be implemented by anything that wants to provide a store for the inverted indexes.

use error::{IndexError,DocumentError};
use document::Document;
use occurrence::Occurrence;

pub trait IndexStore {
    fn exists(name: &str) -> Result<bool, IndexError>;
    fn save(&self) -> Result<(), IndexError>;
    fn add_term(&self, term: &str) -> Result<(), IndexError>;
    fn terms(&self) -> Result<Vec<String>, IndexError>;
    fn add_document(&self, document: &Document) -> Result<(), IndexError>;
    fn add_occurrence(&self, term: &str, occurrence: &Occurrence) -> Result<(), IndexError>;
    fn occurrences_for_term(&self, term: &str) -> Result<Vec<Occurrence>, IndexError>;
    fn count_terms(&self) -> Result<u32, IndexError>;
    fn document_by_id(&self, document_id: &str) -> Result<Document, IndexError>;
    fn delete_document_by_id(&self, document_id: &str) -> Result<(), IndexError>;
    fn delete_occurrences_by_document_id(&self, document_id: &str) -> Result<(), IndexError>;
}

pub trait Index {
    fn name(&self) -> &str;
    fn insert_document(&mut self, d: Document) -> Result<(), DocumentError>;
    fn document_by_id(&self, document_id: &str) -> Result<Document, IndexError>;
    fn insert_bulk_document(&mut self, documents: Vec<Document>) -> Result<(), DocumentError>;
    fn occurrences_by_term(&self, term: &str) -> Result<Vec<Occurrence>, IndexError>;
}
