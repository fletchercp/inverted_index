use error;
use sqlite_index_store::SQLiteIndexStore;
use occurrence::Occurrence;
use document::Document;
use index_store::{IndexStore,Index};
use splitter;
use occurrence;
use rusqlite::Connection;

/// Index is an inverted index of terms to documents
pub struct SQLiteIndex {
    name: String,
    data_directory: String,
    pub storage_engine: SQLiteIndexStore,
}

impl SQLiteIndex {
    pub fn new(name: &str, path: &str) -> Result<SQLiteIndex, error::IndexError> {
        match SQLiteIndexStore::new(name, path) {
            Ok(s) => {
                Ok(SQLiteIndex {
                    name: name.to_string(),
                    data_directory: path.to_string(),
                    storage_engine: s,
                })
            },
            Err(e) => {
                Err(error::IndexError::NotYetImplemented)
            }
        }
    }
}

impl Index for SQLiteIndex {
    fn name(&self) -> &str {
        &self.name
    }

    fn insert_document(&mut self, d: Document) -> Result<(), error::DocumentError> {
        let terms = splitter::split_on_whitespace(&d);
        self.storage_engine.add_document(&d);
        for t in terms.iter() {
            self.storage_engine.add_term(&t.value);
            let new_occurrence = occurrence::Occurrence::new(d.id_as_string().clone(), t.offset as u32);
            self.storage_engine.add_occurrence(&t.value, &new_occurrence);
        }
        Ok(())
    }

    fn insert_bulk_document(&mut self, documents: Vec<Document>) -> Result<(), error::DocumentError> {
        let mut conn = Connection::open(self.data_directory.clone() + "/" + &self.name + ".db").unwrap();
        let tx = conn.transaction().unwrap();
        for d in documents {
            self.storage_engine.add_document(&d);
        }
        tx.commit();
        Ok(())
    }

    fn document_by_id(&self, document_id: &str) -> Result<Document, error::IndexError> {
        self.storage_engine.document_by_id(document_id)
    }

    fn occurrences_by_term(&self, term: &str) -> Result<Vec<Occurrence>, error::IndexError> {
        self.storage_engine.occurrences_for_term(term)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn test_bulk_insert() {
        let mut docs = Vec::<Document>::new();
        for i in 0..100 {
            docs.push(Document::new("This is a test"));
        }
        let mut new_index = SQLiteIndex::new("bulk_test", "tests").unwrap();
        let result = new_index.insert_bulk_document(docs);
        assert_eq!(result.is_ok(), true);
    }

    #[bench]
    fn bench_bulk_insert(b: &mut Bencher) {
        let mut new_store = SQLiteIndex::new("bulk_bench", "tests").unwrap();
        b.iter(|| {
            let mut docs = Vec::<Document>::new();
            for i in 0..100 {
                docs.push(Document::new("This is a test"));
            }
            let result = new_store.insert_bulk_document(docs);
        });
    }
}
