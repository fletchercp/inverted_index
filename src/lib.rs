#![feature(use_extern_macros)]
#![feature(test)]
extern crate uuid;
extern crate test;
extern crate rand;
extern crate rusqlite;

#[macro_use]
extern crate serde_derive;
extern crate serde_json;

pub mod index_filestore;
pub mod document;
pub mod index_sqlite;
mod occurrence;
mod splitter;
mod error;
mod segment_manager;
pub mod index_store;
mod sqlite_index_store;
mod queries;
