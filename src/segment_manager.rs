use std::collections::HashMap;
use std::fs::{File, read_dir, remove_file};
use std::path::PathBuf;
use std::io::prelude::*;

use serde_json;

use occurrence;

#[derive(Debug)]
/// Wraps a path and a File, represening a specific Segment
pub struct SegmentFile {
    fh: File,
    path: PathBuf
}

/// Manages the segments of an Index
#[derive(Debug, Default)]
pub struct SegmentManager {
    directory: PathBuf,
    segments: Vec<SegmentFile>
}

impl SegmentManager {
    /// Creates and returns a new SegmentManager
    pub fn new(path: &str) -> SegmentManager {
        let mut pb = PathBuf::new();
        pb.push(path);
        let mut sm = SegmentManager {
            directory: pb,
            segments: Vec::new()
        };
        sm.load_all_segments();
        if sm.segments.len() == 0 {
            println!("No existing segments found, creating one");
            sm.create_segment(0);
        }
        sm
    }

    /// Flushes a hash of terms out to a segment file
    pub fn flush_to_segment(&mut self, generation: usize, terms: &HashMap<String, Vec<occurrence::Occurrence>>) {
        // There really has to be a better way to build these paths...
        let mut filename = String::from("terms_");
        filename.push_str(&generation.to_string());
        filename.push_str(".json");
        let mut path = self.directory.clone();
        path.push(filename);
        let fh = File::create(path.clone());

        match fh {
            Ok(mut v) => {
                let j = serde_json::to_string_pretty(&terms);
                match j {
                    Ok(json_data) => {
                        match v.write(json_data.as_bytes()) {
                            Ok(_) => {
                                let new_segment = SegmentFile{
                                    fh: v,
                                    path: path,
                                };
                                self.segments.insert(0, new_segment);
                            },
                            Err(e) => {
                                println!("There was an error writing segment to disk: {:?}. fh was: {:?}", e, v);
                            }
                        }
                    },
                    Err(e) => {
                        println!("Error: {:?}", e);
                    }
                }
            },
            Err(_) => {}
        }
    }

    /// Returns a count of the number of open segments we have
    pub fn open_segments(&self) -> usize {
        self.segments.len()
    }

    /// Searches all open segments for a term and returns a merged result
    pub fn find_term(&mut self, term: &str) -> HashMap<String, Vec<occurrence::Occurrence>> {
        let mut results: HashMap<String, Vec<occurrence::Occurrence>> = HashMap::new();
        results.insert(term.to_string(), vec![]);
        for segment in self.segments.iter_mut() {
            let mut json_data = String::new();
            let loaded_data = segment.fh.read_to_string(&mut json_data);
            match loaded_data {
                Ok(_) => {
                    let mut cur_result: HashMap<String, Vec<occurrence::Occurrence>> = serde_json::from_str(&json_data).unwrap();
                    if cur_result.contains_key(term) {
                        let mut tmp = cur_result.remove(term).unwrap();
                        results.get_mut(term).unwrap().append(&mut tmp);
                    }
                },
                Err(_) => {}
            }
        }
        results
    }

    /// Creates a new segment file and adds it to the open file handles
    pub fn create_segment(&mut self, generation: usize) -> bool {
        // There really has to be a better way to build these paths...
        let mut filename = String::from("terms_");
        filename.push_str(&generation.to_string());
        filename.push_str(".json");

        let mut path = self.directory.clone();
        path.push(filename);

        let f = File::create(path.clone());
        match f {
            Ok(fh) => {
                self.segments.insert(0, SegmentFile{ fh: fh, path: path.clone()});
                true
            },
            Err(e) => {
                println!("There was an error creating a new segment file: {:?}. Error was: {:?}", path, e);
                false
            }
        }
    }

    /// Merges the two oldest segment files
    pub fn merge_oldest(&mut self, generation: usize) {
        // TODO: This should probably just create a second file handle
        let mut file1 = self.segments.pop().unwrap();
        let mut file2 = self.segments.pop().unwrap();
        let mut json_data1 = String::new();
        let mut json_data2 = String::new();
        let _ = file1.fh.read_to_string(&mut json_data1);
        let _ = file2.fh.read_to_string(&mut json_data2);
        let t1: HashMap<String, Vec<occurrence::Occurrence>> = serde_json::from_str(&json_data1).unwrap();
        let mut t2: HashMap<String, Vec<occurrence::Occurrence>> = serde_json::from_str(&json_data2).unwrap();
        t2.extend(t1);

        match remove_file(file1.path) {
            Ok(_) => {},
            Err(e) => {
                println!("Error removing merged file: {:?}", e);
            }
        }

        match remove_file(file2.path) {
            Ok(_) => {},
            Err(e) => {
                println!("Error removing merged file: {:?}", e);
            }
        }

        let mut filename = String::from("terms_");
        filename.push_str(&generation.to_string());
        filename.push_str(".json");
        let mut path = self.directory.clone();
        path.push(filename);

        let f = File::create(path.clone());
        match f {
            Ok(fh) => {
                self.segments.insert(0, SegmentFile{ fh: fh, path: path.clone()});
            },
            Err(e) => {
                println!("There was an error creating a new segment file: {:?}. Error was: {:?}", path, e);
            }
        }
    }

    /// Opens all the existing segment files found in an index directory
    pub fn load_all_segments(&mut self) {
        let paths = read_dir(&self.directory).unwrap();
        for p in paths {
            match p {
                Ok(fh) => {
                    let fh2 = File::open(fh.path());
                    match fh2 {
                        Ok(f) => {
                            self.segments.insert(0, SegmentFile{fh: f, path: fh.path()});
                        },
                        Err(e) => {
                            println!("ERROR: Cannot open segment file: {:?}", e);
                        },
                    }
                },
                Err(e) => {
                    println!("ERROR: Cannot open segment file! Error: {:?}", e);
                }
            }

        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::DirBuilder;
    use rand::{Rng ,thread_rng};

    fn random_index_name() -> String {
        return thread_rng().gen_ascii_chars().take(10).collect::<String>();
    }

    fn generate_test_segments(key_name: &str) -> HashMap<String, Vec<occurrence::Occurrence>> {
        let mut test_terms: HashMap<String, Vec<occurrence::Occurrence>>;
        let test_occurrence = occurrence::Occurrence::new("0".to_string(), 1);
        test_terms = HashMap::new();
        test_terms.insert(key_name.to_string(), vec![test_occurrence]);
        test_terms
    }

    fn create_test_segments_directory(idx: &str) -> String {
        let mut p = String::from("/tmp/indices/");
        p.push_str(idx);
        p.push_str("/segments/");
        let _result = DirBuilder::new()
            .recursive(true)
            .create(p.clone()).unwrap();
        let o1 = generate_test_segments("test1");
        let o2 = generate_test_segments("test2");

        let mut fh = File::create(p.clone()+"segment_1.json").unwrap();
        let serialized = serde_json::to_string_pretty(&o1).unwrap();
        let _save_result = fh.write_all(serialized.as_bytes());
        let mut fh = File::create(p.clone()+"segment_2.json").unwrap();
        let serialized = serde_json::to_string_pretty(&o2).unwrap();
        let _save_result = fh.write_all(serialized.as_bytes());
        return p;
    }

    #[test]
    fn test_new_segment_manager() {
        let idx_name = &random_index_name();
        let path = create_test_segments_directory(&idx_name);
        let _new_index = SegmentManager::new(&path);
        assert_eq!(0, 0);
    }

    #[test]
    fn merge_oldest() {
        let idx_name = &random_index_name();
        let path = create_test_segments_directory(&idx_name);
        let mut sm = SegmentManager::new(&path);
        sm.merge_oldest(10);
        assert_eq!(0, 0);
    }

    #[test]
    fn create_segment() {
        let idx_name = &random_index_name();
        let path = create_test_segments_directory(&idx_name);
        let mut sm = SegmentManager::new(&path);
        sm.create_segment(10);
        assert_eq!(3, sm.open_segments());
    }

    #[test]
    fn find_term() {
        let idx_name = &random_index_name();
        let path = create_test_segments_directory(&idx_name);
        let mut sm = SegmentManager::new(&path);
        let results = sm.find_term("test1");
        println!("Results are: {:?}", results);
        assert_eq!(0, 0);
    }

}
