/// This is an index store that keeps its data in SQLite databases
use rusqlite::{Connection,Statement};
use rusqlite;
use index_store::IndexStore;
use error::{IndexError,DocumentError};
use queries::*;
use document::Document;
use occurrence::Occurrence;
use uuid;
use splitter;

pub struct SQLiteIndexStore {
    path: String,
    name: String,
    pub conn: Connection,
}

impl SQLiteIndexStore {
    /// Creates and returns a new SQLiteIndexStore. This store uses SQLite to
    /// store its data.
    pub fn new(name: &str, path: &str) -> Result<SQLiteIndexStore, IndexError> {
        let final_path = path.to_string() + "/" + name + ".db";

        match Connection::open(final_path) {
            Ok(c) => {

                let store = SQLiteIndexStore {
                    name: name.to_string(),
                    path: path.to_string(),
                    conn: c,
                };
                match store.initialize_tables() {
                    Ok(_) => {
                        Ok(store) },
                    Err(e) => {
                        println!("Unable to initialize tables: {:?}", e);
                        Err(IndexError::CannotLoadMetadata) }
                }
            },
            Err(e) => {
                println!("Unable to initialize tables: {:?}", e);
                Err(IndexError::CannotLoadMetadata)
            }
        }
    }

    /// Wrapper function that calls all the table initialization functions. If any generate
    /// an error, that error is returned.
    fn initialize_tables(&self) -> Result<(), rusqlite::Error> {
        match self.initialize_metadata_table() {
            Ok(_) => {},
            Err(e) => {
                println!("Error initializing metadata table: {:?}", e);
                return Err(e); }
        };

        match self.initialize_terms_table() {
            Ok(_) => {},
            Err(e) => {
                println!("Error initializing terms table: {:?}", e);
                return Err(e);
            }
        };

        match self.initialize_documents_table() {
            Ok(_) => {},
            Err(e) => {
                println!("Error initializing documents table: {:?}", e);
                return Err(e);
            }
        };

        match self.initialize_occurrences_table() {
            Ok(_) => {},
            Err(e) => {
                println!("Error initializing occurrences table: {:?}", e);
                return Err(e);
            }
        };

        match self.set_metadata() {
            Ok(_) => {},
            Err(e) => {
                println!("Error setting metadata: {:?}", e);
                return Err(e);
            }
        };

        Ok(())
    }

    /// Initializes the metadata table in the SQLite database
    fn initialize_metadata_table(&self) -> Result<i32, rusqlite::Error> {
        self.conn.execute(QUERY_CREATE_METADATA_TABLE, &[])
    }

    /// Initializes the terms table in the SQLite database
    fn initialize_terms_table(&self) -> Result<i32, rusqlite::Error> {
        self.conn.execute(QUERY_CREATE_TERMS_TABLE, &[])
    }

    /// Initializes the documents table in the SQLite database
    fn initialize_documents_table(&self) -> Result<i32, rusqlite::Error> {
        self.conn.execute(QUERY_CREATE_DOCUMENTS_TABLE, &[])
    }

    /// Initializes the occurrences table in the SQLite database
    fn initialize_occurrences_table(&self) -> Result<i32, rusqlite::Error> {
        self.conn.execute(QUERY_CREATE_OCCURRENCES_TABLE, &[])
    }

    /// Sets the metadata on the ta
    fn set_metadata(&self) -> Result<(), rusqlite::Error> {
        let mut stmt = self.conn.prepare(QUERY_INIT_METADATA).unwrap();
        match stmt.query(&[&self.name, &"false".to_string()]) {
            Ok(_) => {
                return Ok(());
            },
            Err(e) => {
                return Err(e);
            }
        };
    }
}

impl IndexStore for SQLiteIndexStore {
    fn exists(name: &str) -> Result<bool, IndexError> {
        println!("Attempt to check if index exists with name: {:?}", name);
        Err(IndexError::NotYetImplemented)
    }

    fn save(&self) -> Result<(), IndexError> {
        Err(IndexError::NotYetImplemented)
    }

    fn add_term(&self, term: &str) -> Result<(), IndexError> {
        let mut stmt = self.conn.prepare_cached(QUERY_INSERT_TERM).unwrap();
        match stmt.execute(&[&term]) {
            Ok(_) => {
                return Ok(());
            },
            Err(e) => {
                println!("Unable to initialize tables: {:?}", e);
                return Err(IndexError::CannotInsertTerm);
            }
        };
    }

    fn add_document(&self, document: &Document) -> Result<(), IndexError> {
        let mut stmt = self.conn.prepare(QUERY_INSERT_DOCUMENT).unwrap();
        match stmt.execute(&[&document.id_as_string(), &document.value]) {
            Ok(_) => {
                return Ok(());
            },
            Err(e) => {
                println!("Unable to add document: {:?}", e);
                return Err(IndexError::CannotInsertTerm);
            }
        };
    }

    fn add_occurrence(&self, term: &str, occurrence: &Occurrence) -> Result<(), IndexError> {
        let mut stmt = self.conn.prepare_cached(QUERY_INSERT_OCCURRENCE).unwrap();
        match stmt.execute(&[&term, &occurrence.document, &(occurrence.offset as i32)]) {
            Ok(_) => {
                return Ok(());
            },
            Err(e) => {
                println!("Error adding ocurrence: {:?}", e);
                return Err(IndexError::CannotInsertTerm);
            }
        };
    }

    fn terms(&self) -> Result<Vec<String>, IndexError> {
        let mut stmt = self.conn.prepare(QUERY_ALL_TERMS).unwrap();
        let mut results = Vec::<String>::new();
        let iter = stmt.query_map(&[], |row| {
            row.get(0)
        }).unwrap();

        for i in iter {
            results.push(i.unwrap());
        }
        Ok(results)
    }

    fn occurrences_for_term(&self, term: &str) -> Result<Vec<Occurrence>, IndexError> {
        let mut stmt = self.conn.prepare(QUERY_OCCURRENCES_FOR_TERM).unwrap();
        let mut results = Vec::<Occurrence>::new();
        let iter = stmt.query_map(&[&term], |row| {
            Occurrence {
                document: row.get(0),
                offset: row.get(1),
            }
        }).unwrap();
        for i in iter {
            results.push(i.unwrap());
        }
        Ok(results)
    }

    fn count_terms(&self) -> Result<u32, IndexError> {
        let mut stmt = self.conn.prepare(QUERY_COUNT_TERMS).unwrap();
        let result: u32 = stmt.query_row(&[], |r| {
            r.get(0)
        }).unwrap();
        Ok(result)
    }

    fn document_by_id(&self, document_id: &str) -> Result<Document, IndexError> {
        let mut stmt = self.conn.prepare(QUERY_DOCUMENT_BY_ID).unwrap();
        let result: Document = stmt.query_row(&[&document_id.to_string()], |row| {
            let document_uuid: String = row.get(0);
            Document {
                id: uuid::Uuid::parse_str(&document_uuid).unwrap(),
                value: row.get(1),
            }
        }).unwrap();
        Ok(result)
    }

    fn delete_document_by_id(&self, document_id: &str) -> Result<(), IndexError> {
        let mut stmt = self.conn.prepare(QUERY_DELETE_DOCUMENT_BY_ID).unwrap();
        match stmt.execute(&[&document_id.to_string()]) {
            Ok(c) => {
                Ok(())
            },
            Err(e) => {
                println!("Error deleting document ({:?}): {:?}", document_id, e);
                Err(IndexError::NotYetImplemented)
            }
        }
    }

    fn delete_occurrences_by_document_id(&self, document_id: &str) -> Result<(), IndexError> {
        let mut stmt = self.conn.prepare(QUERY_DELETE_OCCURRENCES_BY_DOCUMENT_ID).unwrap();
        match stmt.execute(&[&document_id.to_string()]) {
            Ok(c) => {
                println!("Deleted occurences with document id {:?}", document_id);
                Ok(())
            },
            Err(e) => {
                println!("Error deleting occurrences with document id ({:?}): {:?}", document_id, e);
                Err(IndexError::NotYetImplemented)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::{File,remove_file};
    use document::Document;
    use test::Bencher;
    use std::io::BufRead;
    use std::io::BufReader;

    #[test]
    fn create_store() {
        let new_store = SQLiteIndexStore::new("test", "tests");
        assert_eq!(new_store.is_ok(), true);
        let new_store = new_store.unwrap();
        let result = new_store.add_term("term");
        assert_eq!(result.is_ok(), true);
        let new_document = Document::new("this is a test document");
        let result = new_store.add_document(&new_document);
        assert_eq!(result.is_ok(), true);
        let occurrence = Occurrence::new(new_document.id_as_string(), 0);
        let result = new_store.add_occurrence("term", &occurrence);
        assert_eq!(result.is_ok(), true);
        let result = new_store.count_terms();
        assert_eq!(result.is_ok(), true);
        let result = result.unwrap();
        assert_eq!(result, 1);
        let result = new_store.delete_occurrences_by_document_id(&new_document.id_as_string());
        assert_eq!(result.is_ok(), true);
        let result = new_store.delete_document_by_id(&new_document.id_as_string());
        assert_eq!(result.is_ok(), true);
    }

    #[bench]
    fn bench_sqlite_store_index_single(b: &mut Bencher) {
        let mut new_store = SQLiteIndexStore::new("bench1", "tests").unwrap();
        b.iter(|| {
            let new_document = Document::new("This is a test");
            new_store.add_document(&new_document);
        });
    }
}
