use std::fmt;
use std::error::Error;

#[derive(Debug)]
pub enum DocumentError {
    TermNotFound,
    AddDocumentFailed,
}

impl fmt::Display for DocumentError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            DocumentError::TermNotFound => write!(f, "No matching term was found in the index"),
            DocumentError::AddDocumentFailed => write!(f, "Adding the Document to the Index failed")
        }
    }
}

impl Error for DocumentError {
    fn description(&self) -> &str {
        match *self {
            DocumentError::TermNotFound => "Term not found in Index",
            DocumentError::AddDocumentFailed => "Adding the Document to the Index failed"
        }
    }
}

#[derive(Debug)]
pub enum IndexError {
    DataDirectoryNotFound,
    CannotCreateDirectories,
    CannotLoadMetadata,
    NotYetImplemented,
    CannotInsertTerm,
    // IndexCreationFailed(String),
    // IndexLoadFailed(String),
    // IndexExistCheck(String),
    // IndexSaveError(String),
}

impl fmt::Display for IndexError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            IndexError::DataDirectoryNotFound => write!(f, "No index data path found"),
            IndexError::CannotCreateDirectories => write!(f, "Unable to create directory"),
            IndexError::CannotLoadMetadata => write!(f, "Cannot load metadata"),
            IndexError::NotYetImplemented => write!(f, "Not yet implemented"),
            IndexError::CannotInsertTerm => write!(f, "Cannot insert term")
        }
    }
}

impl Error for IndexError {
    fn description(&self) -> &str {
        match *self {
            IndexError::DataDirectoryNotFound => "No index data path found",
            IndexError::CannotCreateDirectories => "Unable to create directory",
            IndexError::CannotLoadMetadata => "Cannot load metadata",
            IndexError::NotYetImplemented => "Not yet implemented",
            IndexError::CannotInsertTerm => "Cannot insert term"
        }
    }
}
