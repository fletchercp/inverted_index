use uuid;

/// Document is a text string to be tokenized and indexed
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Document {
    pub id: uuid::Uuid,
    pub value: String
}

impl Document {
    /// Creates and returns a new Document
    pub fn new(s: &str) -> Document {
        Document{
            id: uuid::Uuid::new_v4(),
            value: s.to_string()
        }
    }

    /// Returns the ID of the document as a String
    pub fn id_as_string(&self) -> String {
        self.id.hyphenated().to_string()
    }

    /// Returns a reference to the value of the Document
    pub fn value_as_ref(&self) -> &str {
        &self.value
    }
}
