pub static QUERY_CREATE_METADATA_TABLE: &'static str =
    "CREATE TABLE IF NOT EXISTS metadata (
        name                TEXT PRIMARY KEY,
        keep_raw            BOOLEAN NOT NULL,
        UNIQUE(name)
    )";

pub static QUERY_CREATE_TERMS_TABLE: &'static str =
    "CREATE TABLE IF NOT EXISTS terms (
        term    TEXT PRIMARY KEY,
        UNIQUE(term)
    )";

pub static QUERY_CREATE_DOCUMENTS_TABLE: &'static str =
    "CREATE TABLE IF NOT EXISTS documents (
        id TEXT PRIMARY KEY,
        content TEXT
    )";

pub static QUERY_CREATE_OCCURRENCES_TABLE: &'static str =
    "CREATE TABLE IF NOT EXISTS occurrences (
        term TEXT,
        document TEXT,
        offset INTEGER NOT NULL,
        FOREIGN KEY(term) REFERENCES terms(term),
        FOREIGN KEY(document) REFERENCES documents(id)
    )";

pub static QUERY_INSERT_TERM: &'static str =
    "INSERT OR IGNORE INTO terms (term) VALUES (?1)";
pub static QUERY_INSERT_OCCURRENCE: &'static str =
    "INSERT INTO occurrences (term, document, offset) VALUES (?1, ?2, ?3)";
pub static QUERY_INSERT_DOCUMENT: &'static str =
    "INSERT INTO documents (id, content) VALUES (?1, ?2)";
pub static QUERY_INIT_METADATA: &'static str =
    "INSERT OR IGNORE INTO metadata (name, keep_raw) VALUES (?1, ?2)";
pub static QUERY_ALL_TERMS: &'static str =
    "SELECT (term) FROM terms";
pub static QUERY_OCCURRENCES_FOR_TERM: &'static str =
    "SELECT document, offset FROM occurrences WHERE term = ?1";
pub static QUERY_COUNT_TERMS: &'static str =
    "SELECT COUNT(*) FROM terms";
pub static QUERY_DOCUMENT_BY_ID: &'static str =
    "SELECT (id, content) FROM documents WHERE id = ?1";
pub static QUERY_DELETE_DOCUMENT_BY_ID: &'static str =
    "DELETE FROM documents WHERE id = ?1";
pub static QUERY_DELETE_OCCURRENCES_BY_DOCUMENT_ID: &'static str =
    "DELETE FROM occurrences WHERE document = ?1";
