use std::collections::HashMap;
use std::fs::{DirBuilder};
use std::fs::{File,remove_dir_all};
use std::io::prelude::*;
use std::io::{Error};
use std::path::Path;

use serde_json;

use document;
use splitter;
use occurrence;
use error;
use error::{IndexError,DocumentError};
use segment_manager;
use index_store::{Index};
use document::Document;
use occurrence::Occurrence;

/// Index is an inverted index of terms to documents
#[derive(Debug, Serialize, Deserialize)]
pub struct FileStoreIndex {
    name: String,
    metadata: IndexMetadata,
    documents: HashMap<String, document::Document>,
    terms: HashMap<String, Vec<occurrence::Occurrence>>,
    data_directory: String,
    #[serde(skip_serializing, skip_deserializing)]
    bytes_buffered: usize,
    #[serde(skip_serializing, skip_deserializing)]
    segment_manager: segment_manager::SegmentManager
}

/// Used to serialize/deserialize the contents of the segment generation file
#[derive(Debug, Serialize, Deserialize)]
struct IndexMetadata {
    current_generation: usize,
    current_document_id: usize,
    max_bytes: usize,
    // Determines if the raw document should be kept after indexing it
    keep_raw: bool,
}

impl FileStoreIndex {
    /// Returns a new Index
    pub fn new(name: &str, path: &str) -> Result<FileStoreIndex, error::IndexError> {
        let exists = FileStoreIndex::exists(name, path);
        if !exists {
            let result = FileStoreIndex::create_directories(name, path);
            if !result.is_ok() {
                return Err(error::IndexError::CannotCreateDirectories)
            };
            let new_index = FileStoreIndex{
                name: name.to_string(),
                metadata: IndexMetadata{
                    current_generation: 0,
                    current_document_id: 0,
                    max_bytes: 1000000,
                    keep_raw: false,
                },
                documents: HashMap::new(),
                terms: HashMap::new(),
                data_directory: path.to_string(),
                bytes_buffered: 0,
                segment_manager: segment_manager::SegmentManager::new(&FileStoreIndex::segment_directory(name, path)),
            };
            new_index.save_metadata();
            return Ok(new_index);
        }
        FileStoreIndex::load_existing(name, path)
    }

    /// This attempts to load an existing index that is already on disk
    pub fn load_existing(name: &str, data_path: &str) -> Result<FileStoreIndex, error::IndexError> {
        let metadata = FileStoreIndex::load_metadata(name, data_path);
        match metadata {
            Ok(md) => {
                let new_index = FileStoreIndex{
                    name: name.to_string(),
                    metadata: md,
                    documents: HashMap::new(),
                    terms: HashMap::new(),
                    data_directory: data_path.to_string(),
                    bytes_buffered: 0,
                    segment_manager: segment_manager::SegmentManager::new(&FileStoreIndex::segment_directory(name, data_path)),
                };
                return Ok(new_index);
            },
            Err(_) => {
                return Err(error::IndexError::CannotLoadMetadata);
            }
        }
    }

    /// Checks if an index exists locally or not
    pub fn exists(name: &str, data_path: &str) -> bool {
        return Path::new(&format!("{}/indices/{}/", data_path, name)).exists();
    }



    /// Returns how many documents are in the Index
    pub fn count_documents(&self) -> usize {
        self.documents.len()
    }

    /// Returns how many terms are in the Index
    pub fn count_terms(&self) -> usize {
        self.terms.len()
    }

    /// This retrieves a term from the index and returns a reference to its vector of occurrences or an error
    pub fn find_term(&mut self, term: &str) -> HashMap<String, Vec<occurrence::Occurrence>> {
        self.segment_manager.find_term(term)
    }

    /// Changes the max bytes before flush for this index
    pub fn set_max_bytes(&mut self, max_bytes: usize) {
        self.metadata.max_bytes = max_bytes;
    }

    /// Sends the existing terms Hash to the segment manager to be saved to disk
    pub fn flush_buffer(&mut self) {
        if self.terms.len() > 0 {
            self.segment_manager.flush_to_segment(self.metadata.current_generation, &self.terms);
            self.metadata.current_generation += 1;
            self.terms.drain();
            self.save_metadata();
        }
    }

    /// Deletes an Index and all its associated data from the local disk
    pub fn delete_index(name: &str, data_path: &str) -> bool {
        let path = format!("{}/indices/{}/", data_path, name);
        let result = remove_dir_all(path);

        match result {
            Ok(_) => {
                println!("Deleted index {:?}", name);
                true
            },
            Err(e) => {
                println!("Error deleting index {:?}. Error was {:?}", name, e);
                false
            },
        }
    }

    /// Creates the needed directory structure inside the data directory if they don't already exist
    fn create_directories(name: &str, data_path: &str) -> Result<(), Error> {
        let r = FileStoreIndex::create_documents_directory(name, data_path);
        if !r.is_ok() {
            return r;
        };

        let r = FileStoreIndex::create_segments_directory(name, data_path);
        if !r.is_ok() {
            return r;
        };

        return Ok(());
    }

    /// This creates the documents directory where the raw documents are stored
    fn create_documents_directory(name: &str, data_path: &str) -> Result<(), Error> {
        let path = format!("{}/indices/{}/documents/", data_path, name);
        DirBuilder::new().recursive(true).create(path)
    }

    /// Creates the segments directory for the index
    fn create_segments_directory(name: &str, data_path: &str) -> Result<(), Error> {
        let path = format!("{}/indices/{}/segments/", data_path, name);
        DirBuilder::new().recursive(true).create(path)
    }

    /// Loads the current metadata for the index
    fn load_metadata(name: &str, data_path: &str) -> Result<IndexMetadata, error::IndexError> {
        let path = format!("{}/indices/{}/metadata.json", data_path, name);
        let f = File::open(path.clone());
        match f {
            Ok(mut fh) => {
                let mut json_data = String::new();
                let loaded_data = fh.read_to_string(&mut json_data);
                match loaded_data {
                    Ok(_) => {
                        let md: Result<IndexMetadata, serde_json::Error> = serde_json::from_str(&json_data);
                        match md {
                            Ok(v) => {
                                return Ok(v);
                            },
                            Err(e) => {
                                println!("Error deserializing index metadata: {:?}", e);
                                return Err(error::IndexError::CannotLoadMetadata);
                            }
                        }
                    },
                    Err(e) => {
                        println!("Error reading data to string: {:?}", e);
                        return Err(error::IndexError::CannotLoadMetadata);
                    }
                }

            },
            Err(e) => {
                println!("Error opening metadata file: {:?} at {:?}", e, path);
                return Err(error::IndexError::CannotLoadMetadata);
            },
        }
    }

    /// Saves the current index metadata to a file
    fn save_metadata(&self) -> bool {
        let path = format!("{}/indices/{}/metadata.json", self.data_directory, self.name);
        let f = File::create(path);
        match f {
            Ok(mut fh) => {
                let serialized = serde_json::to_string_pretty(&self.metadata);
                match serialized {
                    Ok(s) => {
                        let save_result = fh.write_all(s.as_bytes());
                        match save_result {
                            Ok(_) => {
                                return true;
                            },
                            Err(e) => {
                                println!("Error saving current generation: {:?}", e);
                                return false;
                            },
                        }
                    }
                    Err(e) => {
                        println!("Error saving current generation: {:?}", e);
                        return false;
                    }
                }
            },
            Err(e) => {
                println!("Error saving current generation: {:?}", e);
                return false;
                // TODO: Should return a GenerationFileNotFound error?
            },
        }
    }

    /// Returns the segment directory if given an index name and data path
    fn segment_directory(name: &str, data_path: &str) -> String {
        format!("{}/indices/{}/segments/", data_path, name)
    }
}

impl Index for FileStoreIndex {
    fn name(&self) -> &str {
        &self.name
    }

    fn occurrences_by_term(&self, term: &str) -> Result<Vec<Occurrence>, IndexError> {
        Ok(vec![])
    }

    fn insert_bulk_document(&mut self, documents: Vec<Document>) -> Result<(), error::DocumentError> {
        Ok(())
    }
    /// Returns a document by ID
    fn document_by_id(&self, document_id: &str) -> Result<Document, IndexError> {
        match self.documents.get(&document_id.to_string()) {
            Some(d) => {
                Ok(d.clone())
            },
            None => {
                Err(IndexError::NotYetImplemented)
            },
        }
    }

    /// Adds a document to the Index
    fn insert_document(&mut self, d: document::Document) -> Result<(), error::DocumentError> {
        if self.metadata.keep_raw {
        // First save the raw Document before indexing
            let document_file = File::create(self.data_directory.clone() + "/indices/" + &self.name + "/documents/" + &d.id_as_string());
            self.bytes_buffered += d.value.len();
            match document_file {
                Ok(mut fh) => {
                    if self.metadata.keep_raw {
                        let _save_result = fh.write_all(d.value.as_bytes());
                    }
                },
                Err(_) => {
                    return Err(error::DocumentError::AddDocumentFailed);
                }
            }
        }
        let terms = splitter::split_on_whitespace(&d);

        for t in terms.iter() {
            let new_occurrence = occurrence::Occurrence::new(d.id_as_string().clone(), t.offset as u32);
            if !self.terms.contains_key(&t.value) {
                self.terms.insert(t.value.clone(), Vec::new());
            }
            let found_term = self.terms.get_mut(&t.value);
            match found_term {
                Some(term) => {
                    term.push(new_occurrence);
                },
                None => {
                    return Err(error::DocumentError::TermNotFound);
                }
            }
        }
        self.documents.insert(d.id_as_string(), d);
        self.metadata.current_document_id += 1;
        if self.bytes_buffered >= self.metadata.max_bytes {
            self.save_metadata();
            self.flush_buffer();
            self.bytes_buffered = 0;
        }

        if self.segment_manager.open_segments() > 10 {
            self.segment_manager.merge_oldest(self.metadata.current_generation);
            self.metadata.current_generation += 1;
            self.save_metadata();
        }
        Ok(())
    }

}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::{thread_rng, Rng};
    use test::Bencher;

    use std::io::BufReader;

    fn random_index_name() -> String {
        return thread_rng().gen_ascii_chars().take(10).collect::<String>();
    }

    #[test]
    fn new_index() {
        let _new_index = FileStoreIndex::new(&random_index_name(), "/tmp").unwrap();
    }

    #[test]
    fn test_new_index() {
        let mut new_index = FileStoreIndex::new(&random_index_name(), "/tmp").unwrap();
        let new_document = document::Document::new("This is a test");
        new_index.insert_document(new_document).unwrap();
        assert_eq!(new_index.count_documents(), 1);
    }

    #[test]
    fn test_delete_index() {
        let name = random_index_name();
        let _ = FileStoreIndex::new(&name, "/tmp").unwrap();
        FileStoreIndex::delete_index(&name, "/tmp");
        let exists = FileStoreIndex::exists(&name, "/tmp");
        assert_eq!(exists, false);
    }

    #[test]
    fn test_get_document() {
        let mut new_index = FileStoreIndex::new(&random_index_name(), "/tmp").unwrap();
        let new_document = document::Document::new("This is a test");
        new_index.insert_document(new_document.clone()).unwrap();
        let d = new_index.document_by_id(&new_document.id_as_string());
        assert_eq!(d.unwrap().value_as_ref(), "This is a test");
    }

    #[test]
    fn test_split_document() {
        let mut new_index = FileStoreIndex::new(&random_index_name(), "/tmp").unwrap();
        let new_document = document::Document::new("This is");
        new_index.insert_document(new_document).unwrap();
        assert_eq!(new_index.count_terms(), 2);
    }

    #[test]
    fn test_incorrect_directory_fails() {
        let new_index = FileStoreIndex::new(&random_index_name(), "/cannot_exist");
        match new_index {
            Ok(_) => {
                assert_eq!(1, 0);
            }
            Err(_) => {
                assert_eq!(0, 0);
            }
        }
    }

    #[test]
    fn test_segments() {
        let mut new_index = FileStoreIndex::new(&random_index_name(), "/tmp").unwrap();
        let new_document = document::Document::new("this is");
        new_index.insert_document(new_document).unwrap();
        let new_document2 = document::Document::new("this is yet another test");
        new_index.insert_document(new_document2).unwrap();
        let new_document3 = document::Document::new("and here we go again");
        new_index.insert_document(new_document3).unwrap();
        assert_eq!(0, 0);
    }

    #[test]
    fn test_open_segments() {
        let mut new_index = FileStoreIndex::new(&random_index_name(), "/tmp").unwrap();
        let new_document = document::Document::new("this is");
        new_index.insert_document(new_document).unwrap();
        let new_document2 = document::Document::new("this is yet another test");
        new_index.insert_document(new_document2).unwrap();
        let new_document3 = document::Document::new("and here we go again");
        new_index.insert_document(new_document3).unwrap();
        assert_eq!(0, 0);
    }

    #[bench]
    fn bench_index_single(b: &mut Bencher) {
        let mut new_index = FileStoreIndex::new(&random_index_name(), "/tmp").unwrap();
        let new_document = document::Document::new("This is a test");
        b.iter(||
            new_index.insert_document(new_document.clone())
        );
    }

    #[bench]
    fn bench_index_user_data(b: &mut Bencher) {
        b.iter(|| {
            let mut idx = FileStoreIndex::new("userbench", "/tmp").unwrap();
            let fh = File::open("tests/user_data.txt").unwrap();
            let file = BufReader::new(&fh);

            for line in file.lines() {
               let l = line.unwrap();
               let doc = document::Document::new(&l.to_string());
               let _ = idx.insert_document(doc);
            }
        }

        );
    }
}
